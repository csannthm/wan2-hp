<!doctype html>
<html lang="de" dir="ltr">
   <head>
      <?php
         include "inc/head.inc";
      ?>      
   </head>
   <body>
      <?php
         include "inc/topbar.inc";
         include "inc/body_beforemain.inc";
      ?>
      <div class="grid-x grid-padding-x">
         <div class="large-12 cell"  id="contactcontainer">
            <br />
            <h2 class="name">Kontakt</h2>
            <hr />

            <div class="grid-x grid-padding-x">
               <div class="small-12 medium-6 cell">
                  <div class="card">
                     <img src="img/onur.png">
                     <div class="card-section card-section-center">
                        <ul class="menu">
                           <li class="social-icon">
                              <a href="https://www.facebook.com/profile.php?id=100021747824725" target="_blank">
                                 <i class="fi-social-facebook"></i> 
                              </a>
                           </li>
                           <li class="social-icon">
                              <a href="https://www.instagram.com/onursahin169/" target="_blank">
                                 <i class="fi-social-instagram"></i> 
                              </a>
                           </li>
                           <li class="social-icon">
                              <a href="mailto:onur.sahin@iem.thm.de" target="_blank">
                                 <i class="fi-mail"></i> 
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="small-12 medium-6 cell">
                  <div class="card">
                     <img src="img/chris.png">
                     <div class="card-section card-section-center">
                        <ul class="menu">
                           <li class="social-icon">
                              <a href="https://www.facebook.com/profile.php?id=100010284691546" target="_blank">
                                 <i class="fi-social-facebook"></i> 
                              </a>
                           </li>
                           <li class="social-icon">
                              <a href="mailto:christopher.sann@iem.thm.de" target="_blank">
                                 <i class="fi-mail"></i> 
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

         </div>
         
      </div>
      <?php
         include "inc/body_aftermain.inc";
         include "inc/scripts_body.inc";
      ?>
   </body>
</html>



