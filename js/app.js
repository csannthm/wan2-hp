$(document).foundation();

$(document).ready(function() {

	$(".comment-created-time").each(function() {

		var currentCreatedTaskTime = $(this);
		var createdTime = currentCreatedTaskTime.html();

		var relativeTime = timeDifference(new Date(), withoutTimezoneOffset(new Date(createdTime)));
		currentCreatedTaskTime.show().html(relativeTime);

	});

});