var colon = false;
timer();
var timerInterval = setInterval(timer, 1000);

function timer() {
	let date = new Date();
	let currentDate = new Date(date.getTime());
	let endDate = new Date("<?php echo json_decode($taskEndDateJson) ?>");
	let timeremaining = endDate - currentDate;
	
	if (timeremaining >= 0) {  

		var tasktimeremainingD = Math.floor((timeremaining) / 1000 / 60 / 60 / 24);
		if(tasktimeremainingD <= 0) tasktimeremainingD = "";
		else {
			tasktimeremainingD += " ";
			tasktimeremainingD != 1 ? tasktimeremainingD += "Tage" : tasktimeremainingD += "Tag";
			tasktimeremainingD += " | ";
		}

		var tasktimeremainingH = Math.floor((timeremaining) / 1000 / 60 / 60) % 24;
		tasktimeremainingH = tasktimeremainingH <= 9 ? "0" + tasktimeremainingH : tasktimeremainingH;
		if (tasktimeremainingD <= 0 && tasktimeremainingH <= 0) tasktimeremainingH = "";
		else tasktimeremainingH += ":";

		var tasktimeremainingM = Math.floor((timeremaining) / 1000 / 60) % 60;
		tasktimeremainingM = tasktimeremainingM <= 9 ? "0" + tasktimeremainingM : tasktimeremainingM ;
		tasktimeremainingM += ':';

		var tasktimeremainingS = Math.floor((timeremaining) / 1000) % 60;
		tasktimeremainingS = tasktimeremainingS <= 9 ? "0" + tasktimeremainingS : tasktimeremainingS ;	

		$("#tasktime").html(tasktimeremainingD + tasktimeremainingH + tasktimeremainingM + tasktimeremainingS);	  
	}
	else {
		$("#tasktime").html("Task abgelaufen");
		clearInterval(timerInterval);
	}
}