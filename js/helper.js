function withoutTimezoneOffset(date) {
	return new Date(date.getTime() - (date.getTimezoneOffset() * 60 * 1000));
}

function dateToIso(dateStr) {
	// nur das "T" im Date String
	return dateStr.toString().replace(" ", "T");
}

function timeDifference(current, previous) {
    
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

	var elapsed = current - previous;
    
    if (elapsed < msPerMinute) {

    	elapsedSeconds = Math.round(elapsed / 1000);

    	if(elapsedSeconds < 3) {
    		return "Jetzt";
    	} else {
    		
			if (Math.round(elapsedSeconds) != 1) return "Vor " + elapsedSeconds + " Sekunden";
			else return "Vor 1 Sekunde";
    	}

    } else if (elapsed < msPerHour) {
		if (Math.round(elapsed / msPerMinute) != 1) return "Vor " + Math.round(elapsed / msPerMinute) + " Minuten";
		else return "Vor 1 Minute";
    } else if (elapsed < msPerDay ) {
		if (Math.round(elapsed / msPerHour) != 1) return "Vor " + Math.round(elapsed / msPerHour) + " Stunden";
		else return "Vor 1 Stunde";
    } else if (elapsed < msPerMonth) {
		if (Math.round(elapsed / msPerDay) != 1) return "Vor " + Math.round(elapsed / msPerDay) + " Tage";
		else return "Vor 1 Tag";
    } else if (elapsed < msPerYear) {
		if (Math.round(elapsed / msPerMonth) != 1) return "Vor " + Math.round(elapsed / msPerMonth) + " Monate";
		else return "Vor 1 Monat";
    } else {
     	return "Vor " + Math.round(elapsed / msPerYear)  + " Jahren"; 
    }

}