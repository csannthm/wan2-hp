<?php
	header('Content-Type: application/javascript');
	$milestones = $_GET["milestones"] ?? "[]";
	$milestones = !is_array(json_decode($milestones)) ? "[]" : $milestones;
?>

var milestones = <?php echo $milestones ?>;

console.log(milestones);

$.each( milestones, function( i, val ) {
	var height = 40;
	var lastRectHeight = (i === milestones.length - 1) ? 25 : height;
	$("#task-milestone-container").append('<li><div class="grid-x"> <div class="small-1 cell"><svg height="'+height+'" width="20"><rect x="9" y="0" width="2" height="'+lastRectHeight+'" fill="#000"></rect><circle cx="10" cy="10" r="7" stroke="#000" strokewidth="1" fill="#fff"></circle></svg></div> <div class="small-11 cell"> <span>'+val+'</span> </div> </div></li>');
});