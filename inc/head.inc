<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Pigdo</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
<link rel="stylesheet" href="bower_components/foundation-sites/dist/css/foundation.min.css" />
<link rel="stylesheet" href="bower_components/foundation-icon-fonts/foundation-icons.css" />
<link rel="stylesheet" href="css/pigdo.css" />
