<!doctype html>
<html lang="de" dir="ltr">
   <head>
   <?php
		include "inc/head.inc";
   ?>      
   </head>
   <body>
      <?php
		include "inc/topbar.inc";
	  ?>
      <?php
		include "inc/body_beforemain.inc";
	  ?>
         <div class="grid-x">	 
			<div class="large-12 contentcontainer cell"  id="indexcontainer">
				<div class="grid-x">
					<div class="small-12 large-8 cell">
						<div class="grid-x">
							<div class="small-12 cell">
								<h1>Willkommen bei PIGDO</h1>
								<h2>Handling your Tasks together</h2>
								<hr />
								<h3>Der Social-Media Aufgabenplaner</h3>
								<p>Pigdo ist dein interaktiver Aufgabenplaner für Unterwegs. Erstelle Tasks und teile sie mit Freunden. So wissen sie immer,
								was bei dir gerade ansteht. Natürlich kannst auch du die Task deiner Freunde einsehen und kommentieren. </p>
							</div>
							<div class="small-12 cell text-center">
								<a href="https://cafefull.de/app-release.apk" class="download-pigdo button">
									<i class="fi-download"></i> Download
								</a>
							</div>
						</div>
					</div>
					<div class="small-12 large-4 cell">
						<div class="row">   
							<div class="small-6 columns text-center">
								<div class="phone">
									<div class="presentmedia-container">
										<img src="img/pigdo-preview.jpg" alt="" class="presentmedia">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
         </div>
      <?php
		include "inc/body_aftermain.inc";
	  ?>
	  <?php
		include "inc/scripts_body.inc";
		echo "\n";
	  ?>
   </body>
</html>

