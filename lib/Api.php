<?php

	require_once __DIR__ . '/../vendor/autoload.php';

	class Api {

		const GET = "GET";
		const POST = "POST";
		const PUT = "PUT";
		//const DELETE = "DELETE";

		const USER_ID_HEADER = "X-User-Id";
		const API_TOKEN_HEADER = "X-Api-Token";
		const DEVICE_ID_HEADER = "X-Device-Id";
		const DEVICE_TOKEN_HEADER = "X-Device-Token";
		const MASTER_TOKEN_HEADER = "X-Master-Token";

		const ACK_KEY = "ack";
		const DATA_KEY = "data";
		const CODE_KEY = "code";
		const ACK_SUCCESS = "success";
		const ACK_FAILURE = "failure";
		const ACK_VALIDATOR = "validator";

		const HEADERS = "headers";
		const FORM_PARAMS = "form_params";

		const TOKEN_PARAM = "token";

		private $apiUrl;
		private $guzzleClient;

	    function __construct() {
	    	$this->apiUrl = 'https://cafefull.de/wan_api/public';
	    	$this->guzzleClient = new \GuzzleHttp\Client();
	    }

	    private function getSystemUserHeaders() {
	    	return [
				self::USER_ID_HEADER => "0",
				self::API_TOKEN_HEADER => "8c83ddf6ba472810760a7b196431e27daafdd086",
				self::DEVICE_ID_HEADER => "e8bf44f9de7e4b439",
				self::DEVICE_TOKEN_HEADER => "aef4d8fbcb0957955df49beb8b1962e8be29aeee",
				self::MASTER_TOKEN_HEADER => "be031e59428f8036d3a570510b92e723d60a43587839d9e998f05806faa036ac038d29f4cfd19a70"
			];
	    }

	    public function completeRegistration($userID, $registrationToken) {
	        return json_decode($this->guzzleClient->request(self::PUT, $this->apiUrl . "/users/" . (int) $userID . "/registration", [
	            self::HEADERS => self::getSystemUserHeaders(),
	            self::FORM_PARAMS => [
	            	self::TOKEN_PARAM => $registrationToken
	            ]
	        ])->getBody(), true); 
	    }

	    public function showTask($taskID) {
	        return json_decode($this->guzzleClient->request(self::GET, $this->apiUrl . "/tasks/" . (int) $taskID, [
	            self::HEADERS => self::getSystemUserHeaders(),
	            self::FORM_PARAMS => []
	        ])->getBody(), true); 
	    }

	    public function showTaskComments($taskID) {
	        return json_decode($this->guzzleClient->request(self::GET, $this->apiUrl . "/tasks/" . (int) $taskID . "/comments", [
	            self::HEADERS => self::getSystemUserHeaders(),
	            self::FORM_PARAMS => []
	        ])->getBody(), true); 
	    }

	}


?>