<?php

	class Helper {

		const FOUNDATION_CALLOUT_SUCCESS = "success";
		const FOUNDATION_CALLOUT_WARNING = "warning";

		public static function foundationCallout($type, $title, $msg) {
			echo '<div class="callout '.$type.'" data-closable>
			  <h3>'.$title.'</h3>
			  '.htmlentities($msg).'
			</div>';	
		}

		/* Muss über JS gelöst werden wegen der Timezone....
		public static function timeDifference($current, $previous) {

		    $msPerMinute = 60 * 1000;
		    $msPerHour = $msPerMinute * 60;
		    $msPerDay = $msPerHour * 24;
		    $msPerMonth = $msPerDay * 30;
		    $msPerYear = $msPerDay * 365;
		  
			//$elapsed = $current - $previous;
			var_dump(DateTimeZone::getOffset());
			exit;

			$elapsed = $current->getTimestamp() - $previous->getTimestamp();
		    
		    if ($elapsed < $msPerMinute) {

		    	$elapsedSeconds = round($elapsed / 1000);

		    	if($elapsedSeconds < 3) {
		    		return "Jetzt";
		    	} else {
		    		return "Vor " . $elapsedSeconds . " Sekunden";
		    	}

		    } else if ($elapsed < $msPerHour) {
		         return "Vor " . round($elapsed / $msPerMinute) . " Minuten"; 
		    } else if ($elapsed < $msPerDay ) {
		        return "Vor " . round($elapsed / $msPerHour)  . " Stunden";
		    } else if ($elapsed < $msPerMonth) {
		         return "Vor " . round($elapsed / $msPerDay)  . " Tagen";  
		    } else if ($elapsed < $msPerYear) {
		        return "Vor " . round($elapsed / $msPerMonth) . " Monaten"; 
		    } else {
		     	return "Vor " . round($elapsed / $msPerYear)  . " Jahren"; 
		    }

		}
		*/
	}

?>