# README - [PIGDO-HOMEPAGE](https://cafefull.de/hp/ "PIGDO-HOMEPAGE") #
Diese ReadMe beinhaltet eine Zusammenfassung bezüglich der Ordnerstruktur innerhalb der [Homepage](https://cafefull.de/hp/ "Homepage"), die für die Entwickler und Bewerter dieses Projektes relevant sind.

Die [Homepage](https://cafefull.de/hp/ "Homepage") dient zur Allgemeinen Information, zum Download der App sowie zum öffentlichen Anzeigen von geteilten Tasks.

###[/ (root)](https://bitbucket.org/csannthm/wan2-hp/src "/ (root)")

In diesem Ordner liegen die wesentlichen Homepage-Dateien:

* [contact.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/contact.php?at=master&fileviewer=file-view-default "contact.php") - Kontakt-Infoseite
* [detail.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/detail.php?at=master&fileviewer=file-view-default "detail.php") - Detailseite eines Tasks, die mittels Sharing-Funktion erreicht wird.
* [impressum.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/impressum.php?at=master&fileviewer=file-view-default "impressum.php") - Rechtliche Informationen
* [index.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/index.php?at=master&fileviewer=file-view-default "index.php") - Willkommensseite
* [info.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/info.php?at=master&fileviewer=file-view-default "info.php") - Allgemeine Unterseite zum anzeigen von Informationstexten, z.B. einer erfolgreichen oder fehlgeschlagenen Registrierung.

### [/bower_components/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/bower_components/?at=master "/bower_components/") ###

Dies ist ein automatisch generierter Unterordner, der vom Package Manager "[Bower](https://bower.io/ "Bower")" genutzt wird, um Dateien aus externer Quelle zu verwalten.
Hier relevant:

* [/foundation-sites/dist/css/foundation.min.css](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/bower_components/foundation-sites/dist/css/foundation.min.css?at=master&fileviewer=file-view-default "/foundation-sites/dist/css/foundation.min.css") - Grundlegendes Foundation Framework
* [/foundation-icon-fonts/foundation-icons.css](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/bower_components/foundation-icon-fonts/foundation-icons.css?at=master&fileviewer=file-view-default "/foundation-icon-fonts/foundation-icons.css") - Responsive Icons/Symbole

### [/css/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/css/?at=master "/css/") ###

Dieser Unterordner beinhaltet die Stylesheet-Dateien zum steuern des Aussehens der HP

* [pigdo.css](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/css/pigdo.css?at=master&fileviewer=file-view-default "pigdo.css") - Eigene Stylesheet-Anpassungen, die von Foundation abweichen

### [/img/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/img/?at=master "/img/") ###

Bilder, die innerhalb der Webseite lokal benötigt werden, liegen hier ab.
Dies können .jpg, .gif, .png, aber auch Base64-Dateien sein.

### [/inc/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/inc/?at=master "/inc/") ###

Bereiche, die auf jeder Seite benötigt werden und dynamisch mittels z.B. mittels php include Befehl eingebunden werden können, werden in diesen Ordner komponentenbasiert ausgelagert. Ein editieren dieser Dateien zieht im Regelfall eine Änderung auf jeder Homepage-Unterseite nach sich.

* [body_aftermain.inc](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/inc/body_aftermain.inc?at=master&fileviewer=file-view-defaulat "body_aftermain.inc") - Wrapper des Hauptinhaltsbereichs - Abschluss
* [body_beforemain.inc](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/inc/body_beforemain.inc?at=master&fileviewer=file-view-default "body_beforemain.inc") - Wrapper und Definition des Hauptinhaltsbereichs
* [head.inc](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/inc/head.inc?at=master&fileviewer=file-view-default "head.inc") - Metadaten der Homepage
* [scripts_body.inc](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/inc/scripts_body.inc?at=master&fileviewer=file-view-default "scripts_body.inc") - Einzubindende dynamische Skripte
* [topbar.inc](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/inc/topbar.inc?at=master&fileviewer=file-view-default "topbar.inc") - Die Navigationsleiste

### [/js/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/js/?at=master "/js/") ###

In diesem Ordner befinden sich die Javascript-Dateien, die das dynamische Verhalten der Webseite steuern.

* [/vendor](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/vendor/?at=master "/vendor") - Dieser Unterordner ist Teil von Foundation und dient zur Bereitstellung der dynamischen Inhalte dieses Frameworks
* [app.js](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/app.js?at=master&fileviewer=file-view-default "app.js") - Initialisierungsfunktionen
* [helper.js](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/helper.js?at=master&fileviewer=file-view-default "helper.js") - Stellt verschiedene Hilfsfunktionen zur Verfügung.
* [jquery-3.3.1.min.js](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/jquery-3.3.1.min.js?at=master&fileviewer=file-view-default "jquery-3.3.1.min.js") - Die Quelldatei des JQuery-Frameworks
* [main.php](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/main.php?at=master&fileviewer=file-view-default "main.php") - Unterseite, die dynamisch eingebunden wird, um Meilensteine eines Tasks anzuzeigen.
* [timer.js](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/js/timer.js?at=master&fileviewer=file-view-default "timer.js") - Scriptdatei zum steuern des Task-Countdown-Timers

### [/lib/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/lib/?at=master "/lib/") ###

Der lib Ordner beinhaltet:

* das [Api-Handling](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/lib/Api.php?at=master&fileviewer=file-view-default "Api-Handling"), das im Zuge der HP relevant ist.
* Die [Helper](https://bitbucket.org/csannthm/wan2-hp/src/0c99ca6a085b94acbefcf791d36ce74c748fb660/lib/Helper.php?at=master&fileviewer=file-view-default "Helper") Klasse, welche Hilfsmethoden zur Verfügung stellt

### [/vendor/](https://bitbucket.org/csannthm/wan2-hp/src/e79dbd85ef51e47491aba33af6df099e2ad2e08c/vendor/?at=master "/vendor/") ###

Dieser Ordner wird vom HTTP-Package-Manager "[Composer](https://getcomposer.org/ "Composer")" generiert und dient diesem zur Verwaltung seiner Funktionen.