
<?php
	
	require_once __DIR__ . '/lib/Api.php';
	require_once __DIR__ . '/lib/Helper.php';

	$taskID = $_GET['task_id'] ?? "";
	$api = new Api();

?>

<!doctype html>
<html lang="de" dir="ltr">
   <head>
   	<?php include "inc/head.inc" ?>    
   </head>
   <body class="detail-bg">

		<?php include "inc/topbar.inc" ?>

		<div class="grid-container detail-card-bg">

			<?php

				$showTaskResponse = NULL;
				$showTaskCommentsResponse = NULL;
				$errorFound = false;

				$defaultTaskImage = 'img/placeholder_taskpic.png';
				$defaultProfileImage = 'img/placeholder_userpic.png';

				$taskImage = NULL;
				$profileImage = NULL;
				global $milestones;
				$taskName = NULL;
				$taskShortDescription = NULL;
				$taskLongDescription = NULL;
				$taskEndDate = NULL;

				$errMessage = function() use (&$errorFound) {
					Helper::foundationCallout(Helper::FOUNDATION_CALLOUT_WARNING, "Ladefehler", "Der Task scheint nicht mehr zu existieren.");
					$errorFound = true;
				};

				try {

					$showTaskResponse = (object) $api->showTask($taskID);
					$showTaskCommentsResponse = (object) $api->showTaskComments($taskID);

					if($showTaskResponse->ack == Api::ACK_SUCCESS && $showTaskCommentsResponse->ack == Api::ACK_SUCCESS) {

						if(count($showTaskResponse->data[Api::DATA_KEY]) > 0) {

							$taskData = $showTaskResponse->data[Api::DATA_KEY][0];
							$taskImage = $taskData["images"]["cover_image"];
							$profileImage = $taskData["images"]["profile_image"]["small"] ?? $defaultProfileImage;
							$milestones = $taskData["milestones"];
							$taskName = htmlentities($taskData["name"]);
							$taskShortDescription = htmlentities($taskData["short_description"]);
							$taskLongDescription = htmlentities($taskData["long_description"]);
							$taskEndDate = $taskData["end_date"];

							$milestonesTmp = [];
							if(!empty($milestones) && count($milestones) > 0) {
								foreach($milestones as $currentMilestone) {
									$milestonesTmp[] = $currentMilestone["name"];
								} 
							}
							
							$milestones = json_encode($milestonesTmp);

						} else {
							$errMessage();
						}

						$taskCommentsData = NULL;
						if(count($showTaskCommentsResponse->data[Api::DATA_KEY]) > 0) {

							$taskCommentsData = $showTaskCommentsResponse->data[Api::DATA_KEY];

						} 

					} else {
						$errMessage();
					}

				} catch (Exception $e) {

					$errMessage();

				}

			?>

			<?php if(!$errorFound): ?>
	        <div class="grid-x">  

				<div class="large-12 cell" id="task-container">
	               <div class="grid-x">
						<div class="large-12 cell">

							<?php 
								if(!empty($taskImage)) echo '<picture>';
								$srcElem = function($url, $size) {
									return '<source srcset="'.$url.'" media="(max-width: '.$size.')">';
								};
								foreach($taskImage as $taskImageBreakpoint => $taskImageURL) {
									if($taskImageBreakpoint == "small") {
										echo $srcElem($taskImageURL, "100px");
									} else if($taskImageBreakpoint == "medium") {
										echo $srcElem($taskImageURL, "400px");
									} else if($taskImageBreakpoint == "large") {
										echo $srcElem($taskImageURL, "1000px");
									} 
								}
								$taskImageUrl = $taskImage["large"] ?? $defaultTaskImage;
								echo '<img src="'.$taskImageUrl.'" alt="Task-Bild" id="task-image" />';
								if(!empty($taskImage)) echo '</picture>';
							?>

						</div>
						<div class="large-12 cell description-container">
						 <div class="grid-x">
						    <div class="medium-9 large-9 cell">
						    	<p class="task-info"><span id="task-name"><?php echo $taskName ?></span></p>
						    </div>
						    <div class="medium-3 large-3 cell">
						       <div class="">
						        	<p class="task-info right"><span id="tasktime"></span></p>
						       </div>
						    </div>
						 </div>
						</div>
	               </div>
	            </div>	

				<div class="large-12 cell">

					<div class="grid-x">

						<div class="small-12 cell text-center">
							<div id="profile-image-wrapper">
								<img src="<?php echo $profileImage ?>" alt="Profilbild des Taskerstellers" id="profile-image" class="profile-image" />	
							</div>
							<hr />
						</div>

						<?php if(!empty($taskLongDescription)): ?>
						<div class="small-12 cell">
							<div id="long-task-description-wrapper">
								<p><?php echo $taskLongDescription ?></p>	
							</div>
							<hr />
						</div>
						<?php endif; ?>

						<?php if(!empty(str_replace("[]","",$milestones))): ?>
						<div class="small-12 cell">
							<div id="task-milestone-wrapper">
								<ul id="task-milestone-container" class="no-bullet"></ul>
							</div>
							<hr style="margin-top: 0px;" />
						</div>
						<?php endif; ?>

						<?php if(!empty($taskCommentsData)): ?>
						<div class="small-12 cell">
							<div id="task-comment-wrapper">
							<?php 

								$now = new DateTime();

								foreach($taskCommentsData as $taskComment) {

									$profileImage = $taskComment["images"]["profile_image"]["small"] ?? $defaultProfileImage;

									$createdAt = new DateTime($taskComment["created_at"]);

									echo '<div class="grid-x comment">
											<div class="comment-profile-image small-2 cell">
												<img src="'.$profileImage.'" alt="Bild von '.$taskComment["nickname"].'" title="Bild von '.$taskComment["nickname"].'" />
											</div>
											<div class="small-10 comment-text cell">
												'.nl2br(htmlentities($taskComment["text"])).'
											</div>
											<div class="small-12 comment-footer cell">
												<span class="comment-created-time">'.$taskComment["created_at"].'</span>
											</div>
										  </div>
										 '; 

								}

							?>
							</div>
						</div>

						<div class="callout callout-center">
							<p>Hey! Wenn du dir alle Kommentare durchlesen möchtest, hole dir doch unsere App! <br />Dadurch hast du viel mehr Funktionen und kannst Pigdo noch schneller nutzen 😎</p>
								<a href="https://cafefull.de/app-release.apk" class="download-pigdo button">
									<i class="fi-download"></i> Download
								</a>
						</div>

						<?php endif; ?>

					</div>
				</div>

			</div>
			<?php endif; ?>

		</div>

		<?php include "inc/scripts_body.inc" ?>  
		<?php
			$taskEndDateJson = json_encode($taskEndDate);
			echo "<script>";
			include "js/timer.js";
			echo "</script>";
		?>
   </body>
</html>