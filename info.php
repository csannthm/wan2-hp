<?php
	
	require_once __DIR__ . '/lib/Api.php';
	require_once __DIR__ . '/lib/Helper.php';

	$act = $_GET['action'] ?? "";
	$registrationToken = $_GET['token'] ?? "";
	$cutRegistration = @explode("@", $registrationToken);

    $token = $cutRegistration[0] ?? "";
    $userID = $cutRegistration[1] ?? "";
	$api = new Api();

?>
<!doctype html>
<html lang="de" dir="ltr">
   <head>
   <?php include "inc/head.inc" ?>      
   </head>
   <body>
      <?php 
			include "inc/topbar.inc";
			include "inc/body_beforemain.inc";
	  ?>
         <div class="grid-x grid-padding-x">
			<div class="large-12 cell"  id="infocontainer">
			<br />
			<?php

				switch (trim($act)) {
					case 'registration':

						$response = NULL;
						try {

							$response = (object) $api->completeRegistration($userID, $token);

							$title = "Mitgliedschaft";

							if($response->ack == Api::ACK_SUCCESS) {
								Helper::foundationCallout(Helper::FOUNDATION_CALLOUT_SUCCESS, $title, "Die Registrierung wurde erfolgreich abgschlossen. Du kannst dich jetzt einloggen in der Pigdo App!");
							} else {
								Helper::foundationCallout(Helper::FOUNDATION_CALLOUT_WARNING, $title, "Entweder wurde die Registrierung schon abgeschlossen oder es ist ein Fehler passiert!");
							}

						} catch (Exception $e) {

								Helper::foundationCallout(Helper::FOUNDATION_CALLOUT_WARNING, $title, $e->getMessage());

						}

						break;
					case 'formsent':
						echo '<p class="name">Wir haben Post!</p>
						<hr />
						<div class="center"><p>Dein Anliegen wurde erfolgreich an uns versendet und wird in Kürze bearbeitet.<br />
						<a href="index.php">Hier geht&#39;s zur&uuml;ck zum Index!</a></p>';
						break;					
					default:
						echo '<p class="name">Verlaufen?</p>
						<hr />
						<div class="center">
						<p><a href="index.php">Hier geht&#39;s zur&uuml;ck zum Index!</a></p>';
						break;
				}

			?>
			</div>
         </div>
		 </div>
      <?php include "inc/body_aftermain.inc" ?>
	  <?php include "inc/scripts_body.inc" ?>
   </body>
</html>

