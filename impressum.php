<!doctype html>
<html lang="de" dir="ltr">
   <head>
   <?php
		include "inc/head.inc";
   ?>      
   </head>
   <body>
      <?php
		include "inc/topbar.inc";
	  ?>
      <?php
		include "inc/body_beforemain.inc";
	  ?>
         <div class="grid-x grid-padding-x">
			<div class="large-12 cell"  id="impressumcontainer">
			<br />
			<p class="name">Impressum</p>
			<hr />
			<div class="text-left">
				<p>Von: Onur Sahin und Christopher Sann<br />
				Erstellt im Zuge des Wahlpflichtmoduls WAN2</p>
				<p>Technische Hochschule Mittelhessen<br />
				Wilhelm-Leuschner-Straße 13<br />
				61169 Friedberg (Hessen)<br />
				+49 6031 604-0<br />
				info@thm.de</p>
			</div>
			<hr />
			<h1>Rechtliches</h1>
			<p>Diese App und Homepage wurden im Zuge eines Studienprojektes erstellt und veröffentlicht.
			Es ist untersagt, die App ohne Einwilligung der Autoren zu veröffentlichen oder weiterzuverteilen.</p>
			<p>Nutzer sind für ihre veröffentlichten Inhalte selbst verantwortlich. Sollte ein Beitrag gegen geltendes Recht verstoßen,
			behält es sich der Entwickler vor, diesen Beitrag kommentarlos zu löschen und Sie ggf. von der weiteren Nutzung des Dienstes auszuschließen.</p>
			<p>Es besteht keine Garantie auf die Verfügbarkeit des Dienstes.</p>
			<p>Es besteht ferner kein Anspruch auf Ersatz von technischen Schäden, die durch die Nutzung dieses Programms entstehen könnten.</p>
			<h1>Datenschutz</h1>
			<p>Mit den in dieser App nutzbaren Aktionen ist es unter Umständen möglich, Ihre Person zu identifizieren.
			Veröffentlichen Sie nur Informationen, die Sie auch an Fremde weitergeben würden.</p>
			<p>Während der Programmnutzung werden neben den von Ihnen getätigten Angaben möglicherweise auch Ihre IP-Adresse,
			Informationen zu Ihrer Hardware sowie interne Metadaten wie eine Benutzer-ID in der Datenbank gespeichert.
			Diese Angaben dienen zum Bereitstellen der Dienste. Sie werden nicht genutzt, um Rückschlüsse auf Ihre Person zu ziehen.</p>
			</div>
         </div>
      <?php
		include "inc/body_aftermain.inc";
	  ?>
	  <?php
		include "inc/scripts_body.inc";
		echo "\n";
	  ?>
   </body>
</html>

